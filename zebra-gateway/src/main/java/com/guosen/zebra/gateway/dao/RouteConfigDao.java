package com.guosen.zebra.gateway.dao;

import com.guosen.zebra.gateway.route.model.RouteConfig;

import java.util.List;

/**
 * 网关路由DAO
 */
public interface RouteConfigDao {

    /**
     * 获取网关路由规则配置列表
     * @return 网关路由规则配置列表
     */
    List<RouteConfig> getConfigs();
}
