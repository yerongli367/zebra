package com.guosen.zebra.gateway.route.parser;

import com.guosen.zebra.gateway.route.model.RouteConfig;
import com.guosen.zebra.gateway.route.model.RouteInfo;
import mockit.Tested;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.is;

public class UrlPrefixRouteInfoParserTest {

    @Tested
    private UrlPrefixRouteInfoParser urlPrefixRouteInfoParser;

    @Test
    public void testParse() {
        RouteConfig routeConfig = new RouteConfig();
        routeConfig.setServiceName("com.guosen.zebra.sample.FirstService");
        routeConfig.setVersion("1.0.0");
        routeConfig.setUrlPrefix("/firstService");

        Map<String, RouteInfo> urlPrefixRouteInfoMap = UrlPrefixRouteInfoParser.parse(routeConfig);

        RouteInfo routeInfo = urlPrefixRouteInfoMap.get("/firstService");

        assertThat(routeInfo, is(notNullValue()));
        assertThat(routeConfig.getServiceName(), is("com.guosen.zebra.sample.FirstService"));
        assertThat(routeConfig.getUrlPrefix(), is("/firstService"));
        assertThat(routeConfig.getVersion(), is("1.0.0"));
    }

}
